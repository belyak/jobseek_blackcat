from time import sleep
from celery import Celery

app = Celery('tasks', backend='amqp', broker='amqp://guest@localhost//')

@app.task
def perform_oauth(login, password):
    print('Going to perform OAuth for %s/%s' % (login, password))
    sleep(10)
    print('Complete :)')
    return login+password