import json
import os

import tornado.ioloop
import tornado.web

from tasks import perform_oauth

# определим каталог статики как подкаталог static/ в папке проекта:
STATIC_DIR = os.path.join(os.path.dirname(__file__), 'static')


class AddOAuthTask(tornado.web.RequestHandler):
    def put(self):
        """
        Ставит в очередь запрос на выполнение OAuth авторизации
        """
        login = self.get_argument('login', default='some_login@mail.ru')
        password = self.get_argument('password', default='123')

        result = perform_oauth.delay(login, password)
        self.write(json.dumps({'task_id': result.id}))


class CheckOAuthTaskStatus(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        """
        Проверяет статус ранее поставленной задачи
        """
        task_id = self.get_argument('task_id', default='e3761cef-1a53-4a96-923b-c38963ccce62')
        result = perform_oauth.AsyncResult(task_id)

        self.write(json.dumps({'is_complete': result.ready()}))


application = tornado.web.Application([
    (r"/", AddOAuthTask),
    (r"/check", CheckOAuthTaskStatus),
    (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": STATIC_DIR}),
])


if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()